from board import Board


class Backtrack:
    def __init__(self, board: Board, target_position):
        self.pieces = board.pieces
        self.target_position = target_position
        print(f'{self.pieces}\n{self.target_position}')

    def is_valid(self):
        pass

    def generate_moves(self):
        pass

    def apply_move(self, move):
        pass
