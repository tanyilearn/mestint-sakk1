from pieces import Piece


class Board:
    def __init__(self, x, y):
        self.width, self.height = x, y
        self.pieces = []

    def add_piece(self, piece: Piece):
        if not self.is_free(piece.position):
            raise ValueError
        self.pieces.append(piece)
        print(f'new piece added to board')

    def is_free(self, position):
        for piece in self.pieces:
            if piece.position == position:
                return False
        return True
