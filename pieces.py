class Piece:
    def __init__(self, x, y):
        self.position = (x, y)
        print(f'a piece has created on : {self.position}')

    def relative_moves(self):
        raise NotImplementedError


class King(Piece):
    def relative_moves(self):
        return [(dx, dy) for dx in range(-1, 2) for dy in range(-1, 2) if dx != 0 or dy != 0]


class Knight(Piece):
    def relative_moves(self):
        return [(2, 1), (2, -1), (-2, 1), (-2, -1), (1, 2), (1, -2), (-1, 2), (-1, -2)]
