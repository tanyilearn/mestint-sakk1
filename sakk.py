import pieces as pc
import board as b
from backtrack import Backtrack


if __name__ == '__main__':
    board = b.Board(8, 8)
    king = pc.King(5, 1) # F2
    knight = pc.Knight(5, 2) # F3
    target = (7, 6) # H7
    board.add_piece(king)
    board.add_piece(knight)
    gama_state = Backtrack(board, target)

