# Mestint Sakk1


## Mestersége Intelligencia példa

## Feladat
Adott egy sakktábla, amelyre egy huszárt és egy királyt helyezünk az ábrán látható módon. A feladat az, hogy valamelyik figurával a megjelölt mezőre lépjünk. Csak azzal a figurával lehet lépni a sakklépéseknek megfelelően, amelyik éppen ütésben van a másik által.


![Feladat 2.7](image/feladat.png)


## Modellezés

Modellezés lépései:
- megkeresem a fontos részleteket:
    S = {A1, A2, ... ,H7, H8}
    L ⊆ S
    K ⊆ S
- megadjuk a kezdő állapotot:
    k: (KO, LO)
    KO = F2
    LO = F3
- célállapotok halmaza:
    C = {(K, L) | K = H7 ∨ L = H7}
  - operátorok:
      * Előfeltételek:

          Király lépése:
          Ló ütésben van a Királlyal
          L ütésben van K
    
          Ló lépése:
          Király ütésben van a Lóval
          K ütésben van L

      * Utófeltételek:

          Király lépése:
          Ló ütésben van az új Király pozícióval, vagy fordítva
          L ütésben van K' ∨ K' ütésben van L
    
          Ló lépése:
          Király ütésben van az új Ló pozícióval, vagy fordítva
          K ütésben van L' ∨ L' ütésben van K

      O1(K, L) = {(K', L) | K' a K-ból elérhető egy lépéssel ∧ L ütésben van K ∧ K' ≠ L ∧ (L ütésben van K' ∨ K' ütésben van L)}
  
      O2(K, L) = {(K, L') | L' a L-ből elérhető egy lépéssel ∧ K ütésben van L ∧ K ≠ L' ∧ (K ütésben van L' ∨ L' ütésben van K)}
  
      O(K, L) = O1(K, L) ∪ O2(K, L)
    
- Állapottér:
    A = {(K, L) | K, L ∈ S }
    
- További technikai műveletek:

    * Zsákutca(K, L) = O1(K, L) ∪ O2(K, L) = ∅

    * Körfigyelés: például 'closed' listával megoldható. Ha egy új állapot szerepel a closed listában, akkor kör alakult ki.

    * Mélységi (vagy szélességi) korlát.

    * Terminális csúcs: a korábban definiált 'C' célállalapottal ellenőrizhetjük.
